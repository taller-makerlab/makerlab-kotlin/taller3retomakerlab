package com.jorgevicuna.myapplication.room.entity

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "coach")
data class CoachEntity(
    @PrimaryKey (autoGenerate = true)
    @ColumnInfo(name = "id")
    val id: Int,
    val name: String,
    val universidad: String,
    val carrera: String,
    val edadvalor: String,
    val edad: String

)
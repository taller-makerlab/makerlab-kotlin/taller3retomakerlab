package com.jorgevicuna.myapplication.ui.mapper

import com.jorgevicuna.myapplication.room.entity.CoachEntity
import com.jorgevicuna.myapplication.ui.model.Coach


class CoachViewMapper : CoachMapper<Coach, CoachEntity> {

    override fun mapToView(type: CoachEntity): Coach {
        return Coach(
            type.name ,
            type.universidad,
            type.carrera,
            type.edadvalor,
            type.name + type.edadvalor



        )
    }

    override fun mapToUseCase(type: Coach): CoachEntity {
        return CoachEntity(
            0,

            type.name,
            type.universidad,
            type.carrera,
            type.edadvalor,
            type.edad


        )
    }

}
package com.jorgevicuna.myapplication.ui

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.jorgevicuna.myapplication.R
import com.jorgevicuna.myapplication.ui.adapter.CoachAdapter
import com.jorgevicuna.myapplication.ui.model.Coach
import com.jorgevicuna.myapplication.room.CoachRepository
import com.jorgevicuna.myapplication.room.CoachRoomDatabase
import com.jorgevicuna.myapplication.room.entity.CoachEntity
import com.jorgevicuna.myapplication.ui.mapper.CoachViewMapper
import kotlinx.android.synthetic.main.activity_second.*
import com.tuann.floatingactionbuttonexpandable.FloatingActionButtonExpandable
import kotlinx.coroutines.coroutineScope

class SecondActivity :AppCompatActivity(){

    private val  coachAdapter=
        CoachAdapter()
    private lateinit var coachViewModel: CoachViewModel
    private val coachViewMapper = CoachViewMapper()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_second)
        val fab = findViewById<FloatingActionButtonExpandable>(R.id.fab)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val title = intent.getStringExtra("TITULO")
        textView2.text = title
        coachViewModel = ViewModelProvider(this).get(CoachViewModel::class.java)
        observerCoachList()

        fab.setOnClickListener {
            val intent = Intent(this,
                MainActivity::class.java)
            startActivity(intent)
        }
    }
    private fun observerCoachList() {
        coachViewModel.allCoaches.observe(this, Observer { coach ->

            if (coach.isNotEmpty()) {

                val newList = mutableListOf<Coach>()

                coach.forEach {
                    newList.add(coachViewMapper.mapToView(it))
                }

                setRecyclerView(newList)

            } else {

                getListCoach().forEach {
                    coachViewModel.insert(it)
                }

            }
        })
    }
    private fun setRecyclerView(list: List<Coach>){
        rvList.layoutManager = LinearLayoutManager(applicationContext)
        rvList.adapter = coachAdapter
        coachAdapter.setListCoach(list)

    }

    private fun getListCoach(): List<Coach>{

        val list = mutableListOf<Coach>()
        list.add(
            Coach(
                "Jorgito",
                "UPN",
                "Ing.Electrónica",
                "21",

                ""
            )
        )
        list.add(
            Coach(
                "Jhonny",
                "UPN",
                "Ing.Electrónica",
                "25",
                ""
            )
        )
        list.add(
            Coach(
                "Luz",
                "UPN",
                "Ing.Industrial",
                "21",
                ""
            )
        )
        list.add(
            Coach(
                "Jesus",
                "UPN",
                "Ing.Sistemas",
                "27",
                ""
            )
        )
        list.add(
            Coach(
                "Jose",
                "UPN",
                "Ing.Sistemas",
                "24",
                ""
            )
        )
        list.add(
            Coach(
                "Andy",
                "UPN",
                "Ing.Sistemas",
                "24",
                ""
            )
        )
        return list

    }

    override fun onSupportNavigateUp(): Boolean {

        onBackPressed()
        return true

    }
}
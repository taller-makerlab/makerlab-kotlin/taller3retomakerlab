package com.jorgevicuna.myapplication.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.jorgevicuna.myapplication.R
import com.jorgevicuna.myapplication.ui.model.Coach

class CoachAdapter :RecyclerView.Adapter<CoachViewHolder>(){

    private var listCoach = mutableListOf<Coach>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CoachViewHolder {
       val view =  LayoutInflater.from(parent.context).inflate(R.layout.row_coach, parent , false)
        return CoachViewHolder(view)
    }

    override fun getItemCount(): Int {
       return listCoach.size
    }

    override fun onBindViewHolder(holder: CoachViewHolder, position: Int) {
        val coach = listCoach[position]
        holder.name.text = coach.name
        holder.universidad.text = coach.universidad
        holder.carrera.text = coach.carrera
        holder.edad.text = coach.edad
    }

    fun setListCoach(coachList: List<Coach>){
        this.listCoach =coachList.toMutableList()
        notifyDataSetChanged()
    }
}
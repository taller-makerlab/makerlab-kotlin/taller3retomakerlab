package com.jorgevicuna.myapplication.ui.adapter

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.row_coach.view.*

class CoachViewHolder(itemView : View): RecyclerView.ViewHolder(itemView){
    val name : TextView = itemView.tvName
    val universidad : TextView = itemView.tvUniversidad
    val carrera : TextView = itemView.tvCarrera
    val edad : TextView = itemView.tvEdad
}
